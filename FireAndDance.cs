using System;
using System.Drawing;
using Robocode;
using Robocode.Util;


namespace FireAndDance
{
	class FireAndDance : AdvancedRobot
	{
		//Variavel que vai definir se o robo avança ou recua
		bool movingFoward = true;
		public override void Run()
		{
			// Definindo cores
			SetAllColors(Color.OrangeRed);
			

			// Loop 
			while (true)
			{
				//Se localiza no campo de batalha e se direciona para o centro
				if (X < BattleFieldHeight / 2)
				{
					if (Y < BattleFieldWidth / 2)
					{
						SetTurnRight(45 - Heading);
						Ahead(BattleFieldHeight / 2 - X);
					}
					else
					{
						SetTurnRight(135 - Heading);
						Ahead(BattleFieldHeight - X);
					}
				}
				else
				{
					if (Y < BattleFieldWidth / 2)
					{
						SetTurnLeft(45 - Heading);
						Ahead(BattleFieldHeight / 2 - X);
					}
					else
					{
						SetTurnLeft(135 - Heading);
						Ahead(BattleFieldHeight - X);
					}
				}
			
			}
		}
		//Se colidir contra uma parede, muda a direção
		public override void OnHitWall(HitWallEvent e)
		{
			ReverseDirection();
		}
		
		 //Encontrou inimigo: Travar mira e disparar!!
		public override void OnScannedRobot(ScannedRobotEvent evnt)
		{
			//Define o angulo do inimigo em relação à frente do robo
			double angleToEnemy = HeadingRadians + evnt.BearingRadians;
			//Define o quanto o radar vai precisar girar
			double turnToEnemy = Utils.NormalRelativeAngle(angleToEnemy - RadarHeadingRadians);
			double extraTurn = Math.Atan(36 / evnt.Distance) * (turnToEnemy >= 0 ? 1 : -1);
			
			//Movimenta o radar e trava no inimigo
			SetTurnRadarRightRadians(turnToEnemy + extraTurn);
			//Gira o robo 90 graus para atirar lateralmente, facilitando a fuga em confronto direto
			SetTurnRight(evnt.Bearing + 90);
		
			//Dispara
			Atirar(evnt);


		}

		
		//Se bater em um inimigo, Dispare à queima roupa nele!!
		public override void OnHitRobot(HitRobotEvent evnt)
		{
			//Decida quantos graus vai precisar girar o canhão
			double angulo = Heading - GunHeading + evnt.Bearing;
			//Gire o canhão
			TurnGunRight(angulo);
			//Dispare com força
			Fire(3);
			
			
			//Se foi culpa minha, se posicione para um tiro melhor
			if (evnt.IsMyFault)
			{
				TurnLeft(90 - evnt.Bearing);
				
				Back(150);
			}
			//ou tente manter seu movimento
			else
			{
				TurnRight(90 - evnt.Bearing);
				
				Ahead(150);
			}
		}
		//Se for atingido por balas, mude a direção para evitar bots com calculo de tragetória
		public override void OnHitByBullet(HitByBulletEvent evnt)
		{
			ReverseDirection();
		}
		//Mudança de direção
		public void ReverseDirection()
		{
           if (movingFoward)
			{
				Back(Movimento(300));
				movingFoward = false;
			}
			else
			{
				Ahead(Movimento(300));
				movingFoward = true;
			}
		}

		//Travar o canhão no alvo
		public void Atirar(ScannedRobotEvent evnt)
		{
			//Decide a potencia do tiro
			double firePower = FirePower(evnt);
			//Variaveis para definir o quanto o canhão precisará rotacionar
			double absoluteBearing = evnt.BearingRadians + HeadingRadians;
			double linearBearing = absoluteBearing - GunHeadingRadians;

			//Rotacione o canhão até o alvo
			SetTurnGunRightRadians(Utils.NormalRelativeAngle(linearBearing));


			SetFire(firePower);

		}
		//Economia Inteligente de Energia
		public double FirePower(ScannedRobotEvent evnt)
		{
			double firePower = Others == 1 ? 2 : 3;

			if (evnt.Distance > 400)
			{
				firePower = 2;
			}
			else if (evnt.Distance < 200)
			{
				firePower = 4;
			}

			if (Energy < 1)
			{
				firePower = 0.1;
			}
			else if (Energy < 10)
			{
				firePower = 1;
			}

			return firePower;
		}
		//Verifica se o robo está perto da parede antes de se movimentar
		public bool PertoParede(int movimento)
        {
			return (X < 50 + movimento || X > BattleFieldWidth - ( 50 + movimento) || Y < 50 + movimento || Y > BattleFieldHeight - (50 + movimento));
        }
        //Movimenta o robo
		public double Movimento(int movimento)
        {
        	//Se ele estiver perto da parede, muda a direção
            while (PertoParede(movimento))
            {
				return -movimento;
            }
			return movimento;
        }
    }
}




