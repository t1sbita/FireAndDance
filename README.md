# Robô de combate 1v1 em .NET
Robô com mecanismo para evitar colisão com a parede, com sistema de radar travado (ele mantém o radar preso no inimigo), e movimentação lateral para fugas.
## Pontos Fortes:
- Movimentação lateral
- Permite estar em constante movimento enquanto dispara contra os inimigos.
- Caso perca o inimigo do radar, ele faz um novo scan do campo até encontrar o inimigo novamente.
- Muda de direção caso seja atingido, para evitar bots que calculam tragetória. 
## Pontos Fracos:
- Sistema de Defesa fraco
- Não sabe se desviar de balas, sua única defesa é o movimento constante.

## O que aprendi:
1. Utilizar um arquivo externo como referência.
1. Usar o Git dentro do próprio Visual Code Studio.
1. Debug usando programas externos ao Visual Studio.

## O que mais gostei: 
- Ficar pesquisando referências.
- Conseguir aplicar a ideia de alguns robôs no meu, e ver ele derrotando o "original"
- Ver o código funcionando como esperado, sem erros.
- Conseguir analisar o código e descobrir o que estava causando comportamentos inesperados.


